﻿using Xunit;

namespace RationalNumberArithmetic.Tests
{
    public class MatrixShould
    {
        [Fact]
        public void ImplementTostring()
        {
            var matrix = new Matrix(new[,] { { 1, new RationalNumber(2, 3) }, { new RationalNumber(4, 5), 6 } });
            Assert.Equal("[ 1 2/3 ]\n[ 4/5 6 ]", matrix.ToString());
        }

        public class TellIntrisicCharacteristicsOfTheMatrix
        {
            [Fact]
            public void Width_and_Height()
            {
                // Arrrange
                var matrix1 = new Matrix(new[,] { { new RationalNumber(1, 2), new RationalNumber(3, 4) } });
                var matrix2 = new Matrix(new[,] { { new RationalNumber(1, 2), new RationalNumber(3, 4) }, { new RationalNumber(5, 6), new RationalNumber(7, 8) } });

                // Act and Assert
                Assert.Equal(1, matrix1.Height);
                Assert.Equal(2, matrix1.Width);

                Assert.Equal(2, matrix2.Height);
                Assert.Equal(2, matrix2.Width);
            }

            [Fact]
            public void NumberValues()
            {
                // Arrange
                var matrix = new Matrix(new[,] { { new RationalNumber(1, 2), new RationalNumber(3, 4) }, { new RationalNumber(5, 6), new RationalNumber(7, 8) } });

                // Act and Assert
                Assert.Equal(new RationalNumber(1, 2), matrix[0, 0]);
                Assert.Equal(new RationalNumber(3, 4), matrix[0, 1]);
                Assert.Equal(new RationalNumber(5, 6), matrix[1, 0]);
                Assert.Equal(new RationalNumber(7, 8), matrix[1, 1]);
            }

            [Fact]
            public void IsSquare()
            {
                Assert.False(new Matrix(new[,] { { new RationalNumber(1, 2), new RationalNumber(3, 4) } }).IsSquare);

                Assert.True(new Matrix(new[,] { { new RationalNumber(1, 2), new RationalNumber(3, 4) }, { new RationalNumber(5, 6), new RationalNumber(7, 8) } }).IsSquare);
            }

            [Fact]
            public void IsIdentity()
            {
                Assert.False(new Matrix(new RationalNumber[,] { { 0, 1 }, { 1, 0 } }).IsIdentity);

                Assert.True(new Matrix(new RationalNumber[,] { { 1, 0 }, { 0, 1 } }).IsIdentity);
            }

            [Fact]
            public void IsPermutation()
            {
                Assert.False(new Matrix(new RationalNumber[,] { { 1, 2 } }).IsPermutation, "Permutation matrix must be square matrix");

                Assert.False(new Matrix(new RationalNumber[,] { { 1, 2 }, { 0, 1 } }).IsPermutation, "Permutation matrix must only contain 0 and 1.");

                Assert.False(new Matrix(new RationalNumber[,] { { 1, 0 }, { 1, 0 } }).IsPermutation, "Permutation matrix must only contain one 1 on each row and on each column");

                Assert.True(new Matrix(new RationalNumber[,] { { 0, 1 }, { 1, 0 } }).IsPermutation);
            }

            [Fact]
            public void IsUpperTriangular()
            {
                Assert.False(new Matrix(new RationalNumber[,] { { 1, 2 } }).IsUpperTriangular);

                Assert.False(new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 } }).IsUpperTriangular);

                Assert.True(new Matrix(new RationalNumber[,] { { 1, 2 }, { 0, 4 } }).IsUpperTriangular);
            }

            [Fact]
            public void IsLowerTriangular()
            {
                Assert.False(new Matrix(new RationalNumber[,] { { 1, 2 } }).IsLowerTriangular);

                Assert.False(new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 } }).IsLowerTriangular);

                Assert.True(new Matrix(new RationalNumber[,] { { 1, 0 }, { 3, 4 } }).IsLowerTriangular);
            }
        }

        public class ImplementIEquatable
        {
            [Fact]
            public void WithANullMatrix()
            {
                // Arrange
                var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 } });

                // Actand Assert
                Assert.NotEqual(matrix1, null);
            }

            [Fact]
            public void ForTwoMatricesOfDifferentSize()
            {
                // Arrange
                var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 } });
                var matrix2 = new Matrix(new RationalNumber[,] { { 5, 6 }, { 7, 8 }, { 9, 10 } });

                // Actand Assert
                Assert.NotEqual(matrix1, matrix2);
            }

            [Fact]
            public void ForTwoMatricesOfDifferentValues()
            {
                // Arrange
                var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 } });
                var matrix2 = new Matrix(new RationalNumber[,] { { 5, 6 }, { 7, 8 } });

                // Actand Assert
                Assert.NotEqual(matrix1, matrix2);
            }

            [Fact]
            public void ForTwoMatricesOfSameValues()
            {
                // Arrange
                var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 } });
                var matrix2 = new Matrix(new RationalNumber[,] { { 1, new RationalNumber(4, 2) }, { new RationalNumber(9, 3), new RationalNumber(-4, -1) } });

                // Actand Assert
                Assert.Equal(matrix1, matrix2);
            }
        }

        public class Calculate
        {
            public class Sum
            {
                [Fact]
                public void OfTwoMatrices()
                {
                    // Arrange
                    var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
                    var matrix2 = new Matrix(new RationalNumber[,] { { 7, 8 }, { 9, 10 }, { 11, 12 } });
                    var expectedResult = new Matrix(new RationalNumber[,] { { 8, 10 }, { 12, 14 }, { 16, 18 } });

                    // Act
                    var result = matrix1 + matrix2;

                    // Assert
                    Assert.Equal(expectedResult, result);
                }

                [Fact]
                public void ThrowExceptionWhenMatricesAreNotTheSameSize()
                {
                    // Arrange
                    var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
                    var matrix2 = new Matrix(new RationalNumber[,] { { 7, 8 }, { 9, 10 } });

                    // Act and Assert
                    Assert.Throws<IncompatibleMatrixException>(() => { _ = matrix1 + matrix2; });
                }
            }

            public class Difference
            {
                [Fact]
                public void OfTwoMatrices()
                {
                    // Arange
                    var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
                    var matrix2 = new Matrix(new RationalNumber[,] { { 7, 9 }, { 11, 13 }, { 15, 17 } });
                    var expectedResult = new Matrix(new RationalNumber[,] { { -6, -7 }, { -8, -9 }, { -10, -11 } });

                    // Act
                    var result = matrix1 - matrix2;

                    // Assert
                    Assert.Equal(expectedResult, result);
                }

                [Fact]
                public void ThrowExceptionWhenMatricesAreNotTheSameSize()
                {
                    // Arrange
                    var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
                    var matrix2 = new Matrix(new RationalNumber[,] { { 7, 9 }, { 11, 13 } });

                    // Act and Assert
                    Assert.Throws<IncompatibleMatrixException>(() => { _ = matrix1 - matrix2; });
                }
            }

            public class Product
            {
                [Fact]
                public void OfAMatrixAndANumber()
                {
                    // Arrange
                    const int number = -2;
                    var matrix = new Matrix(new RationalNumber[,] { { 1, 2, 3 }, { 4, 5, 6 } });
                    var expectedResult = new Matrix(new RationalNumber[,] { { -2, -4, -6 }, { -8, -10, -12 } });

                    // Act
                    var result = matrix * number;

                    // Assert
                    Assert.Equal(expectedResult, result);
                }

                [Fact]
                public void OfANumberAndAMatrix()
                {
                    // Arrange
                    const int number = -2;
                    var matrix = new Matrix(new RationalNumber[,] { { 1, 2, 3 }, { 4, 5, 6 } });
                    var expectedResult = new Matrix(new RationalNumber[,] { { -2, -4, -6 }, { -8, -10, -12 } });

                    // Act
                    var result = number * matrix;

                    // Assert
                    Assert.Equal(expectedResult, result);
                }

                [Fact]
                public void OfTwoMatrices()
                {
                    // Arrange
                    var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2, 3 }, { 4, 5, 6 } });
                    var matrix2 = new Matrix(new RationalNumber[,] { { 7, 8 }, { 9, 10 }, { 11, 12 } });
                    var expectedResult = new Matrix(new RationalNumber[,] { { 58, 64 }, { 139, 154 } });

                    // Act
                    var result = matrix1 * matrix2;

                    // Assert
                    Assert.Equal(expectedResult, result);
                }

                [Fact]
                public void ThrowExceptionWhenSizesAreNotCompatible()
                {
                    // Arrange
                    var matrix1 = new Matrix(new RationalNumber[,] { { 1, 2, 3 }, { 4, 5, 6 } });
                    var matrix2 = new Matrix(new RationalNumber[,] { { 7, 8 }, { 9, 10 } });

                    // Act and Assert
                    Assert.Throws<IncompatibleMatrixException>(() => { _ = matrix1 * matrix2; });
                }

            }

            public class Opposite
            {
                [Fact]
                public void OfAMatrix()
                {
                    // Arrange
                    var matrix = new Matrix(new RationalNumber[,] { { 1, -2, 3 }, { -4, 5, -6 } });
                    var expectedResult = new Matrix(new RationalNumber[,] { { -1, 2, -3 }, { 4, -5, 6 } });

                    // Act
                    var result = -matrix;

                    // Assert
                    Assert.Equal(expectedResult, result);
                }

                [Fact]
                public void OfTheOppositeOfAMatrix()
                {
                    // Arrange
                    var matrix = new Matrix(new RationalNumber[,] { { 1, -2, 3 }, { -4, 5, -6 } });

                    // Act
                    var result = -(-matrix);

                    // Assert
                    Assert.Equal(matrix, result);
                }
            }

            [Fact]
            public void Transpose()
            {
                // Arrange
                var matrix = new Matrix(new RationalNumber[,] { { 1, 2, 3 }, { 4, 5, 6 } });
                var expectedTransposed = new Matrix(new RationalNumber[,] { { 1, 4 }, { 2, 5 }, { 3, 6 } });

                // Act
                var transposedMatrix = matrix.Transpose();

                // Assert
                Assert.Equal(expectedTransposed, transposedMatrix);
            }

            public class Invert
            {
                [Fact]
                public void AnIdentityMatrix()
                {
                    // Arrange
                    var matrix = Matrix.GetIdentityMatrix(4);
                    var expectedResult = Matrix.GetIdentityMatrix(4);

                    // Act
                    var invertedMatrix = matrix.Invert();

                    // Assert
                    Assert.Equal(expectedResult, invertedMatrix);
                }

                [Fact]
                public void APermutationMatrix()
                {
                    // Arrange
                    var matrix = new Matrix(new RationalNumber[,]
                        { { 1, 0, 0, 0 }, { 0, 0, 0, 1 }, { 0, 1, 0, 0 }, { 0, 0, 1, 0 } });
                    var expectedResult = matrix.Transpose();

                    // Act
                    var invertedMatrix = matrix.Invert();

                    // Assert
                    Assert.Equal(expectedResult, invertedMatrix);
                }

                [Fact]
                public void AnUpperTriangularMatrix()
                {
                    // Arrange
                    var matrix = new Matrix(new RationalNumber[,]
                        { { 1, 2, 3, 4 }, { 0, 5, 6, 7 }, { 0, 0, 8, 9 }, { 0, 0, 0, 10 } });
                    var expectedResult = new Matrix(new[,]
                    {
                        { 1, new RationalNumber(-2, 5), new RationalNumber(-3, 40), new RationalNumber(-21, 400) },
                        { 0, new RationalNumber(1, 5), new RationalNumber(-3, 20), new RationalNumber(-1, 200) },
                        { 0, 0, new RationalNumber(1, 8), new RationalNumber(-9, 80) },
                        { 0, 0, 0, new RationalNumber(1, 10) }
                    });

                    // Act
                    var invertedMatrix = matrix.Invert();

                    // Assert
                    Assert.Equal(expectedResult, invertedMatrix);
                }

                [Fact]
                public void ALowerTriangularMatrix()
                {
                    // Arrange
                    var matrix = new Matrix(new RationalNumber[,]
                        { { 1, 0, 0, 0 }, { 2, 3, 0, 0 }, { 4, 5, 6, 0 }, { 7, 8, 9, 10 } });
                    var expectedResult = new Matrix(
                        new[,]
                        {
                            { 1, 0, 0, 0 },
                            { new RationalNumber(-2, 3), new RationalNumber(1, 3), 0, 0 },
                            { new RationalNumber(-1, 9), new RationalNumber(-5, 18), new RationalNumber(1, 6), 0 },
                            {
                                new RationalNumber(-1, 15), new RationalNumber(-1, 60), new RationalNumber(-3, 20),
                                new RationalNumber(1, 10)
                            }
                        });

                    // Act
                    var invertedMatrix = matrix.Invert();

                    // Assert
                    Assert.Equal(expectedResult, invertedMatrix);
                }

                [Fact]
                public void AStandardMatrix()
                {
                    // Arrange
                    var matrix = new Matrix(new RationalNumber[,] { { 1, 0, 2 }, { 2, -1, 3 }, { 4, 1, 8 } });
                    var expectedResult =
                        new Matrix(new RationalNumber[,] { { -11, 2, 2 }, { -4, 0, 1 }, { 6, -1, -1 } });

                    // Act
                    var invertedMatrix = matrix.Invert();

                    // Assert
                    Assert.Equal(expectedResult, invertedMatrix);
                }
            }

            public class ComputeDeterminant
            {
                [Fact]
                public void OfAMatrix()
                {
                    Assert.Equal(7, new Matrix(new RationalNumber[,] { { 1, -1 }, { 3, 4 } }).GetDeterminant());
                    Assert.Equal(24,
                        new Matrix(new RationalNumber[,] { { 1, 2, 3 }, { 0, 4, 5 }, { 0, 0, 6 } }).GetDeterminant());
                    Assert.Equal(-36,
                        new Matrix(new RationalNumber[,] { { -2, 2, -3 }, { -1, 1, 3 }, { 4, 0, -1 } })
                            .GetDeterminant());
                    Assert.Equal(0, new Matrix(new RationalNumber[,] { { 1, 2 }, { 2, 4 } }).GetDeterminant());
                }

                [Fact]
                public void ThrowExceptionIfMatrixIsNotSquare()
                {
                    var matrix = new Matrix(new RationalNumber[,] { { 1, 2 } });
                    Assert.Throws<NotASquareMatrixException>(() => { _ = matrix.GetDeterminant(); });
                }
            }

            public class ComputePluDecomposition
            {
                [Fact]
                public void OfASquareMatrix()
                {
                    var matrix2 = new Matrix(new RationalNumber[,]
                        { { 1, -2, 2, -3 }, { -1, 2, 4, 7 }, { -3, -6, 26, 2 }, { -3, -9, 0, -9 } });

                    Assert.True(matrix2.PluDecomposition.Permutation.IsPermutation);
                    Assert.True(matrix2.PluDecomposition.Lower.IsLowerTriangular);
                    Assert.True(matrix2.PluDecomposition.Upper.IsUpperTriangular);

                    Assert.Equal(matrix2,
                        matrix2.PluDecomposition.Permutation * matrix2.PluDecomposition.Lower *
                        matrix2.PluDecomposition.Upper);
                }

                [Fact]
                public void ThrowExceptionIfMatrixIsNotSquare()
                {
                    var matrix = new Matrix(new RationalNumber[,] { { 1, 2 } });
                    Assert.Throws<NotASquareMatrixException>(() => { _ = matrix.PluDecomposition; });
                }
            }
        }
    }
}
