﻿using System.Collections.Generic;
using System.Globalization;
using System.Numerics;
using Xunit;

namespace RationalNumberArithmetic.Tests
{
    public class RationalNumberShould
    {
        private static readonly NumberFormatInfo CustomNumberFormatInfo = new NumberFormatInfo()
        {
            NegativeSign = "$",
            PositiveSign = "£",
            NumberDecimalSeparator = "_"
        };

        public class CreateInstance
        {
            [Fact]
            public void FromIntegers()
            {
                // Arrange
                const int numerator = 123;
                const int denominator = 457;

                // Act
                var n = new RationalNumber(numerator, denominator);

                // Assert
                Assert.Equal(numerator, n.Numerator);
                Assert.Equal(denominator, n.Denominator);
            }

            [Fact]
            public void FromBigIntegers()
            {
                // Arrange
                var numerator = BigInteger.Parse("123456789012345678901234567890");
                var denominator = BigInteger.Parse("1594832670147896302531");

                // Act
                var n = new RationalNumber(numerator, denominator);

                // Assert
                Assert.Equal(numerator, n.Numerator);
                Assert.Equal(denominator, n.Denominator);
            }

            [Fact]
            public void SimplifyingFraction()
            {
                // Arrange
                var numerator = BigInteger.Parse("41152263004115226300411522630");
                var denominator = BigInteger.Parse("53161089004929876751");

                // Act
                var rationalNumber = new RationalNumber(3 * numerator, -3 * denominator);

                // Assert
                Assert.Equal(-numerator, rationalNumber.Numerator);
                Assert.Equal(denominator, rationalNumber.Denominator);
            }
        }

        public class Calculate
        {
            public class Opposite
            {
                [Fact]
                public void OfAPositiveNumber()
                {
                    // Arrange
                    const int numerator = 1;
                    const int denominator = 3;
                    var n = new RationalNumber(numerator, denominator);

                    // Act
                    n = -n;

                    // Assert
                    Assert.Equal(-numerator, n.Numerator);
                    Assert.Equal(denominator, n.Denominator);
                }

                [Fact]
                public void OfANegativeNumber()
                {
                    // Arrange
                    const int numerator = -1;
                    const int denominator = 3;
                    var n = new RationalNumber(numerator, denominator);

                    // Act
                    n = -n;

                    // Assert
                    Assert.Equal(-numerator, n.Numerator);
                    Assert.Equal(denominator, n.Denominator);
                }
            }

            public class Addition
            {
                [Fact]
                public void WithPositiveNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(5, 7);

                    // Act
                    var n = a + b;

                    // Assert
                    Assert.Equal(29, n.Numerator);
                    Assert.Equal(21, n.Denominator);
                }

                [Fact]
                public void WithNegativeNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(-5, 7);

                    // Act
                    var n = a + b;

                    // Assert
                    Assert.Equal(-1, n.Numerator);
                    Assert.Equal(21, n.Denominator);
                }

                [Fact]
                public void AndSimplifyFraction()
                {
                    // Arrange
                    var a = new RationalNumber(3, 4);
                    var b = new RationalNumber(7, 4);

                    // Act
                    var n = a + b;

                    // Assert
                    Assert.Equal(5, n.Numerator);
                    Assert.Equal(2, n.Denominator);
                }
            }

            public class Subtraction
            {
                [Fact]
                public void WithPositiveNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(5, 7);

                    // Act
                    var n = a - b;

                    // Assert
                    Assert.Equal(-1, n.Numerator);
                    Assert.Equal(21, n.Denominator);
                }

                [Fact]
                public void WithNegativeNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(-5, 7);

                    // Act
                    var n = a - b;

                    // Assert
                    Assert.Equal(29, n.Numerator);
                    Assert.Equal(21, n.Denominator);
                }

                [Fact]
                public void AndSimplifyFraction()
                {
                    // Arrange
                    var a = new RationalNumber(7, 4);
                    var b = new RationalNumber(1, 4);

                    // Act
                    var n = a - b;

                    // Assert
                    Assert.Equal(3, n.Numerator);
                    Assert.Equal(2, n.Denominator);
                }
            }

            public class Product
            {
                [Fact]
                public void WithPositiveNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(5, 7);

                    // Act
                    var n = a * b;

                    // Assert
                    Assert.Equal(10, n.Numerator);
                    Assert.Equal(21, n.Denominator);
                }

                [Fact]
                public void WithNegativeNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(-5, 7);

                    // Act
                    var n = a * b;

                    // Assert
                    Assert.Equal(-10, n.Numerator);
                    Assert.Equal(21, n.Denominator);
                }

                [Fact]
                public void AndSimplifyFraction()
                {
                    // Arrange
                    var a = new RationalNumber(2, 5);
                    var b = new RationalNumber(5, 4);

                    // Act
                    var n = a * b;

                    // Assert
                    Assert.Equal(1, n.Numerator);
                    Assert.Equal(2, n.Denominator);
                }
            }

            public class Division
            {
                [Fact]
                public void WithPositiveNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(5, 7);

                    // Act
                    var n = a / b;

                    // Assert
                    Assert.Equal(14, n.Numerator);
                    Assert.Equal(15, n.Denominator);
                }

                [Fact]
                public void PositiveNumberByZero()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = RationalNumber.Zero;

                    // Act
                    var n = a / b;

                    // Assert
                    Assert.Equal(1, n.Numerator);
                    Assert.Equal(0, n.Denominator);
                }

                [Fact]
                public void NegativeNumberByZero()
                {
                    // Arrange
                    var a = new RationalNumber(-2, 3);
                    var b = RationalNumber.Zero;

                    // Act
                    var n = a / b;

                    // Assert
                    Assert.Equal(-1, n.Numerator);
                    Assert.Equal(0, n.Denominator);
                }

                [Fact]
                public void WithNegativeNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);
                    var b = new RationalNumber(-5, 7);

                    // Act
                    var n = a / b;

                    // Assert
                    Assert.Equal(-14, n.Numerator);
                    Assert.Equal(15, n.Denominator);
                }

                [Fact]
                public void AndSimplifyFraction()
                {
                    // Arrange
                    var a = new RationalNumber(2, 5);
                    var b = new RationalNumber(4, 5);

                    // Act
                    var n = a / b;

                    // Assert
                    Assert.Equal(1, n.Numerator);
                    Assert.Equal(2, n.Denominator);
                }
            }

            public class Properties
            {
                [Theory]
                [InlineData(1, 2, 1)]
                [InlineData(3, -4, -1)]
                [InlineData(-5, 6, -1)]
                [InlineData(-7, -8, 1)]
                [InlineData(-9, 0, -1)]
                [InlineData(10, 0, 1)]
                public void IndicateSign(int numerator, int denominator, int expectedSign)
                {
                    // Arrange
                    var n = new RationalNumber(numerator, denominator);

                    // Act and Assert
                    Assert.Equal(expectedSign, n.Sign);
                }

                [Theory]
                [InlineData(2, 0, true)]
                [InlineData(-3, 0, true)]
                [InlineData(1, -4, false)]
                public void IndicateInfinity(int numerator, int denominator, bool expectedInfinity)
                {
                    // Arrange
                    var n = new RationalNumber(numerator, denominator);

                    // Act and Assert
                    Assert.Equal(expectedInfinity, n.IsInfinity);
                }

                [Theory]
                [InlineData(2, 1, true)]
                [InlineData(-9, 3, true)]
                [InlineData(1, -4, false)]
                public void IndicateInteger(int numerator, int denominator, bool expectedInteger)
                {
                    // Arrange
                    var n = new RationalNumber(numerator, denominator);

                    // Act and Assert
                    Assert.Equal(expectedInteger, n.IsInteger);
                }

            }

            public class Pow
            {
                [Fact]
                public void WithPositiveNumber()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);

                    // Act
                    var n = a.Pow(5);

                    // Assert
                    Assert.Equal(32, n.Numerator);
                    Assert.Equal(243, n.Denominator);
                }

                [Fact]
                public void WithNegativeNumber()
                {
                    // Arrange
                    var a = new RationalNumber(-2, 3);

                    // Act
                    var n = a.Pow(5);

                    // Assert
                    Assert.Equal(-32, n.Numerator);
                    Assert.Equal(243, n.Denominator);
                }

                [Fact]
                public void WithNegativeNumberAndEvenPow()
                {
                    // Arrange
                    var a = new RationalNumber(-2, 3);

                    // Act
                    var n = a.Pow(4);

                    // Assert
                    Assert.Equal(16, n.Numerator);
                    Assert.Equal(81, n.Denominator);
                }

                [Fact]
                public void With0Pow()
                {
                    // Arrange
                    var a = new RationalNumber(2, 3);

                    // Act
                    var n = a.Pow(0);

                    // Assert
                    Assert.Equal(RationalNumber.One, n);
                }
            }
        }

        public class Compare
        {
            public class Equality
            {
                [Fact]
                public void WithTwoNullInstances()
                {
                    // Arrange
                    var a = null as RationalNumber;
                    var b = null as RationalNumber;

                    // Act
                    var areEqual = a == b;

                    // Assert
                    Assert.True(areEqual);
                }

                [Fact]
                public void WithOnlyOneNullInstance()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = null as RationalNumber;

                    // Act
                    var areEqual = a == b;

                    // Assert
                    Assert.False(areEqual);
                }

                [Fact]
                public void WithTwoDifferentNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 5);

                    // Act
                    var areEqual = a == b;

                    // Assert
                    Assert.False(areEqual);
                }

                [Fact]
                public void WithTwoEqualNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 6);

                    // Act
                    var areEqual = a == b;

                    // Assert
                    Assert.True(areEqual);
                }
            }

            public class Inequality
            {
                [Fact]
                public void WithTwoNullInstances()
                {
                    // Arrange
                    var a = null as RationalNumber;
                    var b = null as RationalNumber;

                    // Act
                    var areDifferent = a != b;

                    // Assert
                    Assert.False(areDifferent);
                }

                [Fact]
                public void WithOnlyOneNullInstance()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = null as RationalNumber;

                    // Act
                    var areDifferent = a != b;

                    // Assert
                    Assert.True(areDifferent);
                }

                [Fact]
                public void WithTwoDifferentNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 5);

                    // Act
                    var areDifferent = a != b;

                    // Assert
                    Assert.True(areDifferent);
                }

                [Fact]
                public void WithTwoEqualNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 6);

                    // Act
                    var areDifferent = a != b;

                    // Assert
                    Assert.False(areDifferent);
                }
            }

            public class LowerThan
            {
                [Fact]
                public void WithTwoNullInstances()
                {
                    // Arrange
                    var a = null as RationalNumber;
                    var b = null as RationalNumber;

                    // Act
                    var aLowerThanB = a < b;

                    // Assert
                    Assert.False(aLowerThanB);
                }

                [Fact]
                public void WithOnlyOneNullInstance()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = null as RationalNumber;

                    // Act
                    var aLowerThanB = a < b;

                    // Assert
                    Assert.False(aLowerThanB);
                }

                [Fact]
                public void WithALowerThanB()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 5);

                    // Act
                    var aLowerThanB = a < b;

                    // Assert
                    Assert.True(aLowerThanB);
                }

                [Fact]
                public void WithAGreaterThanB()
                {
                    // Arrange
                    var a = new RationalNumber(2, 5);
                    var b = new RationalNumber(1, 3);

                    // Act
                    var aLowerThanB = a < b;

                    // Assert
                    Assert.False(aLowerThanB);
                }

                [Fact]
                public void WithTwoEqualNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 6);

                    // Act
                    var aLowerThanB = a < b;

                    // Assert
                    Assert.False(aLowerThanB);
                }
            }

            public class LowerThanOrEqual
            {
                [Fact]
                public void WithTwoNullInstances()
                {
                    // Arrange
                    var a = null as RationalNumber;
                    var b = null as RationalNumber;

                    // Act
                    var aLowerThanOrEqualB = a <= b;

                    // Assert
                    Assert.True(aLowerThanOrEqualB);
                }

                [Fact]
                public void WithOnlyOneNullInstance()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = null as RationalNumber;

                    // Act
                    var aLowerThanOrEqualB = a <= b;

                    // Assert
                    Assert.False(aLowerThanOrEqualB);
                }

                [Fact]
                public void WithALowerThanB()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 5);

                    // Act
                    var aLowerThanOrEqualB = a <= b;

                    // Assert
                    Assert.True(aLowerThanOrEqualB);
                }

                [Fact]
                public void WithAGreaterThanB()
                {
                    // Arrange
                    var a = new RationalNumber(2, 5);
                    var b = new RationalNumber(1, 3);

                    // Act
                    var aLowerThanOrEqualB = a <= b;

                    // Assert
                    Assert.False(aLowerThanOrEqualB);
                }

                [Fact]
                public void WithTwoEqualNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 6);

                    // Act
                    var aLowerThanOrEqualB = a <= b;

                    // Assert
                    Assert.True(aLowerThanOrEqualB);
                }
            }

            public class GreaterThan
            {
                [Fact]
                public void WithTwoNullInstances()
                {
                    // Arrange
                    var a = null as RationalNumber;
                    var b = null as RationalNumber;

                    // Act
                    var aGreaterThanB = a > b;

                    // Assert
                    Assert.False(aGreaterThanB);
                }

                [Fact]
                public void WithOnlyOneNullInstance()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = null as RationalNumber;

                    // Act
                    var aGreaterThanB = a > b;

                    // Assert
                    Assert.False(aGreaterThanB);
                }

                [Fact]
                public void WithALowerThanB()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 5);

                    // Act
                    var aGreaterThanB = a > b;

                    // Assert
                    Assert.False(aGreaterThanB);
                }

                [Fact]
                public void WithAGreaterThanB()
                {
                    // Arrange
                    var a = new RationalNumber(2, 5);
                    var b = new RationalNumber(1, 3);

                    // Act
                    var aGreaterThanB = a > b;

                    // Assert
                    Assert.True(aGreaterThanB);
                }

                [Fact]
                public void WithTwoEqualNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 6);

                    // Act
                    var aGreaterThanB = a > b;

                    // Assert
                    Assert.False(aGreaterThanB);
                }
            }

            public class GreaterThanOrEqual
            {
                [Fact]
                public void WithTwoNullInstances()
                {
                    // Arrange
                    var a = null as RationalNumber;
                    var b = null as RationalNumber;

                    // Act
                    var aGreaterThanOrEqualB = a >= b;

                    // Assert
                    Assert.True(aGreaterThanOrEqualB);
                }

                [Fact]
                public void WithOnlyOneNullInstance()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = null as RationalNumber;

                    // Act
                    var aGreaterThanOrEqualB = a >= b;

                    // Assert
                    Assert.False(aGreaterThanOrEqualB);
                }

                [Fact]
                public void WithALowerThanB()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 5);

                    // Act
                    var aGreaterThanOrEqualB = a >= b;

                    // Assert
                    Assert.False(aGreaterThanOrEqualB);
                }

                [Fact]
                public void WithAGreaterThanB()
                {
                    // Arrange
                    var a = new RationalNumber(2, 5);
                    var b = new RationalNumber(1, 3);

                    // Act
                    var aGreaterThanOrEqualB = a >= b;

                    // Assert
                    Assert.True(aGreaterThanOrEqualB);
                }

                [Fact]
                public void WithTwoEqualNumbers()
                {
                    // Arrange
                    var a = new RationalNumber(1, 3);
                    var b = new RationalNumber(2, 6);

                    // Act
                    var aGreaterThanOrEqualB = a >= b;

                    // Assert
                    Assert.True(aGreaterThanOrEqualB);
                }
            }
        }

        public class Convert
        {
            [Theory]
            [InlineData(123654789)]
            [InlineData(-987654321)]
            [InlineData(0)]
            public void FromInt(int integer)
            {
                // Act
                RationalNumber n = integer;

                // Assert
                Assert.Equal(integer, n.Numerator);
                Assert.Equal(1, n.Denominator);
            }

            [Theory]
            [InlineData(12365478987654321)]
            [InlineData(-98765432123654789)]
            [InlineData(0L)]
            public void FromLong(long longInteger)
            {
                // Act
                RationalNumber n = longInteger;

                // Assert
                Assert.Equal(longInteger, n.Numerator);
                Assert.Equal(1, n.Denominator);
            }

            [Fact]
            public void FromBigInteger()
            {
                // Arrange
                var bigInteger = BigInteger.Parse("123654789654123");

                // Act
                RationalNumber n = bigInteger;

                // Assert
                Assert.Equal(bigInteger, n.Numerator);
                Assert.Equal(1, n.Denominator);
            }

            [Theory]
            [InlineData(0d)]
            [InlineData(1d)]
            [InlineData(1d / 3)]
            [InlineData(0.01234d)]
            public void FromAndToDouble(double doubleValue)
            {
                // Act
                RationalNumber a = doubleValue;

                // Assert
                Assert.Equal(doubleValue, a.DoubleValue);
            }

            public static IEnumerable<object[]> DecimalData => new[]
            {
                new object[]{ 0m },
                new object[]{ 1m },
                new object[]{ 1m /3 },
                new object[]{ 0.01234m }
            };

            [Theory]
            [MemberData(nameof(DecimalData))]
            public void FromAndToDecimal(decimal decimalValue)
            {
                // Act
                RationalNumber a = decimalValue;

                // Assert
                Assert.Equal(decimalValue, a.DecimalValue);
            }

            [Fact]
            public void ToStringWithoutArgument()
            {
                // Arrange
                RationalNumber rationalNumber = new RationalNumber(-123, 457);

                // Act
                var stringValue = rationalNumber.ToString();

                // Assert
                Assert.Equal("-123/457", stringValue);
            }

            [Fact]
            public void ToStringWithCustomFormatProvider()
            {
                // Arrange
                RationalNumber rationalNumber = new RationalNumber(-123, 457);

                // Act
                var stringValue = rationalNumber.ToString(CustomNumberFormatInfo);

                // Assert
                Assert.Equal("$123/457", stringValue);
            }
        }

        public class Parse
        {
            [Fact]
            public void WithInvariantCultureInfo()
            {
                // Arrange
                const string stringValue = "-0.01234567";

                // Act
                var rationalNumber = RationalNumber.Parse(stringValue, CultureInfo.InvariantCulture.NumberFormat);

                // Assert
                Assert.Equal(-1234567, rationalNumber.Numerator);
                Assert.Equal(100000000, rationalNumber.Denominator);
            }

            [Fact]
            public void WithCustomCultureInfo()
            {
                // Arrange
                const string stringValue = "$0_01234567";

                // Act
                var rationalNumber = RationalNumber.Parse(stringValue, CustomNumberFormatInfo);

                // Assert
                Assert.Equal(-1234567, rationalNumber.Numerator);
                Assert.Equal(100000000, rationalNumber.Denominator);
            }

            [Fact]
            public void TryParseSucess()
            {
                // Arrange
                const string stringValue = "-0.01234567";

                // Act
                var parsingSuccess = RationalNumber.TryParse(stringValue, CultureInfo.InvariantCulture.NumberFormat, out var rationalNumber);

                // Assert
                Assert.True(parsingSuccess);
                Assert.Equal(-1234567, rationalNumber.Numerator);
                Assert.Equal(100000000, rationalNumber.Denominator);
            }

            [Fact]
            public void TryParseFail()
            {
                // Arrange
                const string stringValue = "badFormattedNumber";

                // Act
                var parsingSuccess = RationalNumber.TryParse(stringValue, CultureInfo.InvariantCulture.NumberFormat, out var rationalNumber);

                // Assert
                Assert.False(parsingSuccess);
                Assert.Null(rationalNumber);
            }
        }
    }
}
