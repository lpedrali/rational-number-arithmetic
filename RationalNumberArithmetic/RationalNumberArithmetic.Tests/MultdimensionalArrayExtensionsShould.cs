﻿using Xunit;

namespace RationalNumberArithmetic.Tests
{
    public class MultdimensionalArrayExtensionsShould
    {
        [Fact]
        public void ConvertMultidimensionalArrayToJaggedArray()
        {
            // Arrange
            var multidimensionalArray = new[,] { { 1, 2, 3 }, { 4, 5, 6 } };

            // Act
            var jaggedArray = multidimensionalArray.ToJaggedArray();

            // Assert
            Assert.Equal(new[] { new[] { 1, 2, 3 }, new[] { 4, 5, 6 } }, jaggedArray);
        }
    }
}