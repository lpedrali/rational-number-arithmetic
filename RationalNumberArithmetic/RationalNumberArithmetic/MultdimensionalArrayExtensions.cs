﻿namespace RationalNumberArithmetic
{
    public static class MultdimensionalArrayExtensions
    {
        public static T[][] ToJaggedArray<T>(this T[,] matrix)
        {
            var result = new T[matrix.GetLength(0)][];

            for (var i = 0; i < result.Length; i++)
            {
                result[i] = new T[matrix.GetLength(1)];
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    result[i][j] = matrix[i, j];
                }
            }

            return result;
        }
    }
}