﻿using System;
using System.Globalization;
using System.Numerics;
using System.Text.RegularExpressions;

namespace RationalNumberArithmetic
{
    public class RationalNumber : IEquatable<RationalNumber>
    {
        public static readonly RationalNumber Zero = new RationalNumber(BigInteger.Zero, BigInteger.One);
        public static readonly RationalNumber One = new RationalNumber(BigInteger.One, BigInteger.One);

        public readonly BigInteger Numerator;
        public readonly BigInteger Denominator;

        public RationalNumber(BigInteger numerator, BigInteger denominator)
        {
            (Numerator, Denominator) = Simplify(numerator, denominator);
        }

        public RationalNumber(long n) : this(n, 1) { }

        #region Calculation

        public static RationalNumber operator -(RationalNumber a)
            => new RationalNumber(
                -a.Numerator,
                a.Denominator);

        public static RationalNumber operator +(RationalNumber a, RationalNumber b)
            => new RationalNumber(
                a.Numerator * b.Denominator + b.Numerator * a.Denominator,
                a.Denominator * b.Denominator);

        public static RationalNumber operator -(RationalNumber a, RationalNumber b)
            => new RationalNumber(
                a.Numerator * b.Denominator - b.Numerator * a.Denominator,
                a.Denominator * b.Denominator);

        public static RationalNumber operator *(RationalNumber a, RationalNumber b)
            => new RationalNumber(
                a.Numerator * b.Numerator,
                a.Denominator * b.Denominator);

        public static RationalNumber operator /(RationalNumber a, RationalNumber b)
            => new RationalNumber(
                a.Numerator * b.Denominator,
                a.Denominator * b.Numerator);

        public RationalNumber Pow(uint pow)
        {
            var result = RationalNumber.One;
            for (var i = 0; i < pow; i++)
            {
                result *= this;
            }

            return result;
        }

        #endregion

        #region Comparison

        public static bool operator ==(RationalNumber left, RationalNumber right)
            => Equals(left, right);

        public static bool operator !=(RationalNumber left, RationalNumber right)
            => !(left == right);

        public static bool operator <(RationalNumber a, RationalNumber b)
            => a != null && b != null && a.Numerator * b.Denominator < b.Numerator * a.Denominator;

        public static bool operator <=(RationalNumber a, RationalNumber b)
            => a == b || a < b;

        public static bool operator >(RationalNumber a, RationalNumber b)
            => a != null && b != null && !(a <= b);

        public static bool operator >=(RationalNumber a, RationalNumber b)
            => a == b || a > b;

        #endregion

        #region IEquatable

        public bool Equals(RationalNumber other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Numerator.Equals(other.Numerator) && Denominator.Equals(other.Denominator);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((RationalNumber)obj);
        }

        public override int GetHashCode() { unchecked { return (Numerator.GetHashCode() * 397) ^ Denominator.GetHashCode(); } }

        #endregion

        #region Conversion

        public static implicit operator RationalNumber(BigInteger n) => new RationalNumber(n, BigInteger.One);
        public static implicit operator RationalNumber(long n) => new RationalNumber(n, BigInteger.One);
        public static implicit operator RationalNumber(double d)
        {
            if (d == 0) return Zero;

            var pow2Exponent = (int)Math.Floor(Math.Log(Math.Abs(d), 2));
            var pow2 = Math.Pow(2, pow2Exponent);

            d /= pow2;

            var numerator = BigInteger.Zero;
            var denominator = BigInteger.One;

            while (d != 0)
            {
                var digit = (int)(d % 2);
                if (digit != 0)
                {
                    numerator += digit;
                    d -= digit;
                }
                numerator *= 2;
                denominator *= 2;
                d *= 2;
            }

            if (pow2Exponent >= 0)
                numerator *= (int)pow2;
            else
                denominator *= (int)(1.0 / pow2);

            return new RationalNumber(numerator, denominator);
        }
        public static implicit operator RationalNumber(decimal d)
        {
            if (d == 0) return Zero;

            var pow10Exponent = (int)Math.Floor(Math.Log10((double)Math.Abs(d)));
            var pow10 = (decimal)Math.Pow(10, pow10Exponent);

            d /= pow10;

            var numerator = BigInteger.Zero;
            var denominator = BigInteger.One;

            while (d != 0)
            {
                var digit = (int)(d % 10);
                if (digit != 0)
                {
                    numerator += digit;
                    d -= digit;
                }
                numerator *= 10;
                denominator *= 10;
                d *= 10;
            }

            if (pow10Exponent >= 0)
                numerator *= (int)pow10;
            else
                denominator *= (int)(1m / pow10);

            return new RationalNumber(numerator, denominator);
        }

        public double DoubleValue => (double)Numerator / (double)Denominator;

        public decimal DecimalValue => (decimal)Numerator / (decimal)Denominator;

        public static RationalNumber Parse(string s, NumberFormatInfo numberFormatInfo)
        {
            var match = Regex.Match(s.Trim(), $@"^([{Regex.Escape(numberFormatInfo.NegativeSign)}{Regex.Escape(numberFormatInfo.PositiveSign)}]?)(\d+){Regex.Escape(numberFormatInfo.NumberDecimalSeparator)}?(\d*)$");
            if (!match.Success)
                throw new ArgumentException(nameof(s));

            var signPart = match.Groups[1].Value;
            var intPart = match.Groups[2].Value;
            var decPart = match.Groups[3].Value;


            var numerator = BigInteger.Parse(intPart);
            var denominator = BigInteger.One;

            if (!string.IsNullOrEmpty(decPart))
            {
                var pow10 = BigInteger.Pow(new BigInteger(10), decPart.Length);

                denominator *= pow10;

                numerator *= pow10;
                numerator += BigInteger.Parse(decPart);
            }

            if (signPart == numberFormatInfo.NegativeSign)
                numerator = -numerator;

            return new RationalNumber(numerator, denominator);
        }
        public static bool TryParse(string s, NumberFormatInfo numberFormatInfo, out RationalNumber rationalNumber)
        {
            try
            {
                rationalNumber = Parse(s, numberFormatInfo);
                return true;
            }
            catch (Exception)
            {
                rationalNumber = null;
                return false;
            }
        }

        public override string ToString() => ToString(null);

        public string ToString(IFormatProvider provider)
            => Denominator != BigInteger.One
                ? Numerator.ToString(provider) + "/" + Denominator.ToString(provider)
                : Numerator.ToString(provider);

        #endregion

        #region Properties

        public int Sign => Numerator.Sign;
        public bool IsInfinity => Denominator == BigInteger.Zero;
        public bool IsInteger => Denominator == BigInteger.One;

        #endregion

        private static (BigInteger numerator, BigInteger denominator) Simplify(BigInteger numerator, BigInteger denominator)
        {
            if (denominator == BigInteger.Zero)
                return (numerator.Sign, BigInteger.Zero);

            var gcd = BigInteger.GreatestCommonDivisor(numerator, denominator);
            numerator /= gcd;
            denominator /= gcd;

            return denominator < BigInteger.Zero
                ? (-numerator, -denominator)
                : (numerator, denominator);
        }
    }
}
