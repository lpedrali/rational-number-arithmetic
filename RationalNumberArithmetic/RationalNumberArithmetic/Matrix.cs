﻿using System;
using System.Linq;

namespace RationalNumberArithmetic
{
    public class Matrix : IEquatable<Matrix>
    {
        private readonly RationalNumber[,] _numbers;

        public Matrix(int height, int width)
        {
            _numbers = new RationalNumber[height, width];
            for (var i = 0; i < height; i++)
                for (var j = 0; j < width; j++)
                    this[i, j] = RationalNumber.Zero;
        }
        public Matrix(RationalNumber[,] numbers)
        {
            _numbers = numbers;
        }

        public int Height => _numbers.GetLength(0);
        public int Width => _numbers.GetLength(1);

        public bool IsSquare => Height == Width;

        public bool IsIdentity
        {
            get
            {
                if (!IsSquare) return false;

                for (var i = 0; i < Height; i++)
                    for (var j = 0; j < Width; j++)
                        if (this[i, j] != (i == j ? 1 : 0))
                            return false;
                return true;
            }
        }
        public bool IsPermutation
        {
            get
            {
                if (!IsSquare) return false;

                for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                    if (this[i, j] != 0 && this[i, j] != 1)
                        return false;

                for (var i = 0; i < Height; i++)
                {
                    var onePresentOnRow = false;
                    for (var j = 0; j < Width; j++)
                    {
                        if (this[i, j] == 0) continue;
                        if (onePresentOnRow) return false;
                        onePresentOnRow = true;
                    }

                    if (!onePresentOnRow) return false;
                }

                for (var j = 0; j < Width; j++)
                {
                    var onePresentOnColumn = false;
                    for (var i = 0; i < Height; i++)
                    {
                        if (this[i, j] == 0) continue;
                        if (onePresentOnColumn) return false;
                        onePresentOnColumn = true;
                    }

                    if (!onePresentOnColumn) return false;
                }

                return true;
            }

        }
        public bool IsUpperTriangular
        {
            get
            {
                if (!IsSquare) return false;

                for (var i = 0; i < Height; i++)
                    for (var j = 0; j < i; j++)
                        if (this[i, j] != 0)
                            return false;
                return true;
            }
        }
        public bool IsLowerTriangular
        {
            get
            {
                if (!IsSquare) return false;

                for (var i = 0; i < Height; i++)
                    for (var j = i + 1; j < Width; j++)
                        if (this[i, j] != 0)
                            return false;
                return true;
            }
        }

        public RationalNumber this[int x, int y]
        {
            get => _numbers[x, y];
            set
            {
                _numbers[x, y] = value;
                _pluDecomposition = null;
            }
        }

        private PluDecomposition _pluDecomposition;

        public PluDecomposition PluDecomposition
        {
            get
            {
                _pluDecomposition ??= new PluDecomposition(_numbers);
                return _pluDecomposition;
            }
        }

        public RationalNumber GetDeterminant()
        {
            var result = RationalNumber.One;

            for (var i = 0; result != RationalNumber.Zero && i < Height; i++)
                result *= PluDecomposition.Upper[i, i];

            return result;
        }

        public Matrix Transpose()
        {
            var matrix = new Matrix(Width, Height);

            for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                    matrix[j, i] = this[i, j];

            return matrix;
        }

        public Matrix Invert()
        {
            if (IsIdentity)
                return this;

            if (IsPermutation)
                return Transpose();

            if (IsUpperTriangular)
            {
                var dimension = Width;

                var workClone = new Matrix(dimension, dimension);
                for (var i = 0; i < dimension; i++)
                    for (var j = 0; j < dimension; j++)
                    {
                        workClone[i, j] = this[i, j];
                    }

                var result = GetIdentityMatrix(dimension);


                for (var pivotLine = 0; pivotLine < dimension; pivotLine++)
                {
                    var coefficient = 1 / workClone[pivotLine, pivotLine];
                    for (var col = pivotLine; col < dimension; col++)
                    {
                        workClone[pivotLine, col] *= coefficient;
                        result[pivotLine, col] *= coefficient;
                    }
                    for (var currentLine = 0; currentLine < pivotLine; currentLine++)
                    {
                        var coef2 = workClone[currentLine, pivotLine];
                        for (var col = pivotLine; col < dimension; col++)
                        {
                            workClone[currentLine, col] -= coef2 * workClone[pivotLine, col];
                            result[currentLine, col] -= coef2 * result[pivotLine, col];
                        }
                    }
                }

                return result;
            }

            if (IsLowerTriangular)
            {
                return Transpose().Invert().Transpose();
            }

            return PluDecomposition.Upper.Invert() * PluDecomposition.Lower.Invert() * PluDecomposition.Permutation.Invert();
        }

        public static Matrix GetIdentityMatrix(int dimension)
        {
            var result = new Matrix(dimension, dimension);

            for (var i = 0; i < dimension; i++)
                result[i, i] = RationalNumber.One;

            return result;
        }

        #region Operators

        public static Matrix operator +(Matrix a, Matrix b)
        {
            if (a.Height != b.Height || a.Width != b.Width)
                throw new IncompatibleMatrixException();

            var resultNumbers = new RationalNumber[a.Height, a.Width];
            for (var i = 0; i < a.Height; i++)
                for (var j = 0; j < a.Width; j++)
                {
                    resultNumbers[i, j] = a[i, j] + b[i, j];
                }

            return new Matrix(resultNumbers);
        }
        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a.Width != b.Height)
                throw new IncompatibleMatrixException();

            var resultNumbers = new RationalNumber[a.Height, b.Width];
            for (var i = 0; i < a.Height; i++)
                for (var j = 0; j < b.Width; j++)
                {
                    resultNumbers[i, j] = RationalNumber.Zero;
                    for (var k = 0; k < a.Width; k++)
                        resultNumbers[i, j] += a[i, k] * b[k, j];
                }

            return new Matrix(resultNumbers);
        }

        public static Matrix operator -(Matrix a) => a * -RationalNumber.One;
        public static Matrix operator -(Matrix a, Matrix b) => a + (-b);

        public static Matrix operator *(RationalNumber b, Matrix a) => a * b;
        public static Matrix operator *(Matrix a, RationalNumber b)
        {
            var resultNumbers = new RationalNumber[a.Height, a.Width];
            for (var i = 0; i < a.Height; i++)
                for (var j = 0; j < a.Width; j++)
                {
                    resultNumbers[i, j] = b * a[i, j];
                }

            return new Matrix(resultNumbers);
        }


        #endregion

        #region IEquatable

        public bool Equals(Matrix other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            if (this.Height != other.Height || this.Width != other.Width) return false;
            for (var i = 0; i < this.Height; i++)
                for (var j = 0; j < this.Width; j++)
                    if (this[i, j] != other[i, j]) return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Matrix)obj);
        }

        public override int GetHashCode()
        {
            return (_numbers != null ? _numbers.GetHashCode() : 0);
        }

        public static bool operator ==(Matrix left, Matrix right) => Equals(left, right);
        public static bool operator !=(Matrix left, Matrix right) => !Equals(left, right);

        #endregion

        public override string ToString()
            => string.Join("\n", _numbers.ToJaggedArray().Select(row => "[ " + string.Join(" ", row.Select(n => n)) + " ]"));
    }

    public class PluDecomposition
    {
        public readonly Matrix Permutation;
        public readonly Matrix Lower;
        public readonly Matrix Upper;

        public PluDecomposition(RationalNumber[,] numbers)
        {
            if (numbers.GetLength(0) != numbers.GetLength(1))
                throw new NotASquareMatrixException();

            var dimension = numbers.GetLength(0);

            Permutation = Matrix.GetIdentityMatrix(dimension);

            Lower = Matrix.GetIdentityMatrix(dimension);

            Upper = new Matrix((RationalNumber[,])numbers.Clone());

            for (var pivotLine = 0; pivotLine < dimension; pivotLine++)
            {
                if (Upper[pivotLine, pivotLine] == 0)
                {
                    for (var i = pivotLine + 1; i < dimension; i++)
                    {
                        if (Upper[i, pivotLine] != 0)
                        {
                            SwapRows(Upper, pivotLine, i);
                            SwapRows(Permutation, pivotLine, i);
                            (Lower[pivotLine, pivotLine - 1], Lower[i, pivotLine - 1]) = (Lower[i, pivotLine - 1], Lower[pivotLine, pivotLine - 1]);
                            break;
                        }
                    }
                }

                for (var currentLine = pivotLine + 1; currentLine < dimension; currentLine++)
                {
                    var coefficient = Upper[currentLine, pivotLine] / Upper[pivotLine, pivotLine];
                    Lower[currentLine, pivotLine] = coefficient;
                    for (var j = pivotLine; j < dimension; j++)
                    {
                        Upper[currentLine, j] -= (Upper[pivotLine, j] * coefficient);
                    }
                }
            }
        }

        private static void SwapRows(Matrix matrix, int row1Index, int row2Index)
        {
            for (var columnIndex = 0; columnIndex < matrix.Height; columnIndex++)
                (matrix[row2Index, columnIndex], matrix[row1Index, columnIndex])
                    = (matrix[row1Index, columnIndex], matrix[row2Index, columnIndex]);
        }
    }

    public class NotASquareMatrixException : InvalidOperationException
    {
        public NotASquareMatrixException()
            : base("Invalid operation because matrix is not square.") { }
    }

    public class IncompatibleMatrixException : InvalidOperationException
    {
        public IncompatibleMatrixException()
            : base("Operation sannot be performed because matrix is not the good dimension.") { }
    }
}
