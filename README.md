# RationalNumberArithmetic

Library for arithmetic with rational numbers

## What is it

Sometimes floating point calculation is not precise enough, and we need exact calculations.
This library allows to work with rational numbers, to achieve this goal.
